import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'password'
})
export class PasswordPipe implements PipeTransform {

  transform(value: any, ocultar:boolean = true): any {
    let pattern = /./g;
    if(ocultar){
      return value.replace(pattern, '*');
    }
    return value;
  }

}
