import { Component } from '@angular/core';
import {ElementAnimationStyleHandler} from "@angular/animations/browser/src/render/css_keyframes/element_animation_style_handler";
import {DateFormatter} from "@angular/common/src/pipes/deprecated/intl";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nombre:string =  "fernando";
  nombre2:string = "Miguel anGEL chI PEch";
  arrayData: number[] = [1, 2, 3, 4 ,5];
  PI = Math.PI;
  a: number = 0.234;
  salario: number = 2450;
  heroe:any = {
    nombre: 'Logan',
    clave: 'Wolverine',
    edad: 500,
    poderes: ['Regeneracion', 'Fuerza sobrehumana', 'Huesos de adamantium']
  };

  valorPromesa =  new Promise((resolve, reject)=>{
    setTimeout(()=>resolve("Datos resuletos"), 3500);
  });

  fecha = new Date("2018-11-08");

  video = 'W9P_qUnMaFg';

  mostrarPassword:boolean = false;
  contrasenia:string = "Algoritmo!_[?22@";

}
